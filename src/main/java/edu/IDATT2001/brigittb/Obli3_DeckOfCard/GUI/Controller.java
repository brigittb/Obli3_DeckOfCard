package edu.IDATT2001.brigittb.Obli3_DeckOfCard.GUI;

import edu.IDATT2001.brigittb.Obli3_DeckOfCard.GameLogic.DeckOfCards;
import edu.IDATT2001.brigittb.Obli3_DeckOfCard.GameLogic.Hand;
import edu.IDATT2001.brigittb.Obli3_DeckOfCard.GameLogic.PlayingCard;
import edu.IDATT2001.brigittb.Obli3_DeckOfCard.GameLogic.PlayingCardImage;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.FileNotFoundException;
import java.util.ArrayList;

public class Controller{
    Hand cardsAtHand = new Hand();
    ArrayList<ImageView> cardImages;

    @FXML public Button dealHandButton;
    @FXML public Button checkHandButton;
    @FXML public Label sumDisplay;
    @FXML public Label flushDisplay;
    @FXML public Label gameOver;
    @FXML public Label doYouHaveS12Display;
    @FXML public Label filterHearts ;
    @FXML public Label doYouHavereqCardDisplay ;
    @FXML public Button submitReq;
    @FXML public TextField cardValue;
    @FXML public ImageView card1, card2, card3, card4, card5,card6, card7, card8, card9, card10 ;
    @FXML public Label info;
    @FXML public Label info1;
    @FXML public Label info2;

    /**
     * Deals out cards to the hand and
     */
    public void dealHand() throws FileNotFoundException {
        PlayingCard card;
        Image img;

        info.setVisible(false);
        info1.setVisible(false);
        info2.setVisible(false);

        cardsAtHand.addCardsToHand();

        cardImages = new ArrayList<>();
        cardImages.add(card1);
        cardImages.add(card2);
        cardImages.add(card3);
        cardImages.add(card4);
        cardImages.add(card5);
        cardImages.add(card6);
        cardImages.add(card7);
        cardImages.add(card8);
        cardImages.add(card9);
        cardImages.add(card10);

        for(int i = 0; i < cardsAtHand.getCardsAtHandAsCards().size(); i++){
            card = cardsAtHand.getCardsAtHandAsCards().get(i);
            img = PlayingCardImage.getImageUrl(card);
            cardImages.get(i).setImage(img);
            cardImages.get(i).setVisible(true);
        }

        gameOver();

    }

    /**
     * Checks cards at hand for different properties
     */

    public void checkHand() {

        filterHearts.setText("");

        sumDisplay.setText(String.valueOf(cardsAtHand.getSumOfCardsAtHand()));

        if (cardsAtHand.flush()){
            flushDisplay.setText("Flush!");
            flushDisplay.setVisible(true);
            cardsAtHand.removeFromHand(cardsAtHand.getChar());
        } else {
            flushDisplay.setVisible(false);
        }

        if (cardsAtHand.filterOutHearts().size() == 0) filterHearts.setText("No");
        else filterHearts.setText(cardsAtHand.filterOutHearts().toString());

        if(cardsAtHand.doYouHaveSpadesQueen()) doYouHaveS12Display.setText("Yes");
        else doYouHaveS12Display.setText("No");

    }

    /**
     * Checks if you have a card at hand, from the value of textfield
     */

    public void findCard(){
        String value = cardValue.getText();
        if (cardsAtHand.doYouHaveThisCard(value)) doYouHavereqCardDisplay.setText("Yes");
        else doYouHavereqCardDisplay.setText("No");
    }

    public void gameOver(){
        if(cardsAtHand.deckIsEmpty()){
            gameOver.setVisible(true);
        }
    }

    public void reStart(){
        cardsAtHand.clearHand();
        cardsAtHand = new Hand();
        doYouHavereqCardDisplay.setText("");
        gameOver.setVisible(false);
        doYouHaveS12Display.setText("");
        filterHearts.setText("");
        sumDisplay.setText("");
        info.setVisible(true);
        info1.setVisible(true);
        info2.setVisible(true);

        for (ImageView c:
             cardImages) {
            c.setVisible(false);
        }

    }

}

