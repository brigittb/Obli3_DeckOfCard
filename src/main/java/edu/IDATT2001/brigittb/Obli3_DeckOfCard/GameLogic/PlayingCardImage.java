package edu.IDATT2001.brigittb.Obli3_DeckOfCard.GameLogic;

import javafx.scene.image.Image;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class PlayingCardImage extends Image {
    PlayingCard card;

    public PlayingCardImage(Image url, PlayingCard card) {
        super(String.valueOf(url));
        this.card = card;
    }

    public static Image getImageUrl(PlayingCard card) throws FileNotFoundException {
        String url = "src/main/resources/Images/";
        String suit;
        String c = Character.toString(card.getSuit());
        if (c.equals("H")) {
            suit = "Heart";
        } else if (c.equals("S")) {
            suit = "Spades";
        } else if (c.equals("C")) {
            suit = "Clubs";
        }else {
            suit = "Diamond";
        }

        FileInputStream fis = new FileInputStream(url+suit+card.getFace()+ ".png");

        return new Image(fis);
    }

}
