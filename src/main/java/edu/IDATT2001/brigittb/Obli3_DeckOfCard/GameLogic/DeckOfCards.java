package edu.IDATT2001.brigittb.Obli3_DeckOfCard.GameLogic;

import java.util.ArrayList;
import java.util.Random;

public class DeckOfCards{

    private ArrayList<PlayingCard> deckOfCards;
    private final char[] suit = { 'S', 'H', 'D', 'C' };
    private final int[] face = {1,2,3,4,5,6,7,8,9,10,11,12,13};

    public DeckOfCards() {
        this.deckOfCards = new ArrayList<>();
        for(char c: suit){
            for(int i : face){
                deckOfCards.add(new PlayingCard(c, i));
            }
        }
    }


    public ArrayList<PlayingCard> dealHand(int n){
        ArrayList<PlayingCard> dealtHand = new ArrayList<>();
        Random randomCard = new Random();
        PlayingCard card;

        for (int i = 0; i < n; i++) {
            card = deckOfCards.get(randomCard.nextInt(deckOfCards.size()));
            if (!isCardAlreadyDealt(dealtHand,card)){
                dealtHand.add(card);
            }
        }
        return dealtHand;
    }

    public boolean isCardAlreadyDealt(ArrayList<PlayingCard> list, PlayingCard card){
        boolean isDealt = false;

        for (PlayingCard s : list) {
            if (s.equals(card)) {
                isDealt = true;
                break;
            }
        }
        return isDealt;
    }

    public void removeFromDeck(PlayingCard card){
        deckOfCards.remove(card);
    }

    public boolean isDeckEmpty(){
        return deckOfCards.isEmpty();
    }

}
