package edu.IDATT2001.brigittb.Obli3_DeckOfCard.GameLogic;

import java.util.*;
import java.util.stream.Collectors;


public class Hand {
    DeckOfCards deck = new DeckOfCards();
    ArrayList<PlayingCard> cardsAtHand;
    ArrayList<String> type = new ArrayList<>();
    int sum;
    char c;

    public Hand() {
        cardsAtHand = new ArrayList<>(10);
    }

    /**
     * a get method that returns the cards at hand
     *
     * @return an array with the cards at hand
     */
    public ArrayList<PlayingCard> getCardsAtHandAsCards() {
        return cardsAtHand;
    }

    /**
     * Use this to deal cards to the hand. The first hand is dealt with 5 cards,
     * then followed by 2 cards for each time the method is called
     */
    public void addCardsToHand() {
        if (cardsAtHand.size() == 0) {
            cardsAtHand.addAll(deck.dealHand(5));
        } else if (cardsAtHand.size() == 1) {
            cardsAtHand.addAll(deck.dealHand(4));
        } else if (cardsAtHand.size() == 2) {
            cardsAtHand.addAll(deck.dealHand(3));
        } else if (cardsAtHand.size() == 3) {
            cardsAtHand.addAll(deck.dealHand(2));
        } else if (cardsAtHand.size() == 4) {
            cardsAtHand.addAll(deck.dealHand(1));
        }else if(cardsAtHand.size() < 10){
            cardsAtHand.addAll(deck.dealHand(1));
        }else {
            clearHand();
        }

        for (PlayingCard s : cardsAtHand) {
            deck.removeFromDeck(s);
        }
    }

    /**
     * clear the hand of cards
     */
    public void clearHand() {
        cardsAtHand.clear();
    }

    /**
     * Sums up the face of all the cards the player has at hand
     *
     * @return the sum of the cards at hand
     */
    public int getSumOfCardsAtHand() {
        this.sum = 0;
        sum = cardsAtHand.stream().map(PlayingCard::getFace).reduce(0, Integer::sum);
        return sum;


    }

    /**
     * Filters out cards from hand that matches the requested suit
     *
     * @return all cards with matching suit
     */
    public ArrayList<String> filterOutHearts() {
        this.type.clear();

        List<PlayingCard> suits = cardsAtHand.stream().filter(card -> card.getSuit() == 'H').collect(Collectors.toList());

        for (PlayingCard p : suits) {
            type.add(p.getAsString());
        }

        if (type.isEmpty()) type.add("No hearts");

        return type;
    }

    /**
     * do you have a Spades Queen in the cards in you hand?
     *
     * @return boolean true if you have, false if you dont
     */
    public boolean doYouHaveSpadesQueen() {
        return cardsAtHand.stream().anyMatch(q -> q.getSuit() == 'S' && q.getFace() == 12);
    }

    /**
     * Checks if you have a specific card at hand
     *
     * @param req the requested card as a string
     * @return the card
     */
    public boolean doYouHaveThisCard(String req) {

        return cardsAtHand.stream().anyMatch(c -> c.getAsString().equals(req));

    }

    /**
     * Checks if the 5 cards are of the same suit or not
     *
     * @return true if they are, false if they are not
     */
    public boolean flush() {
        Map<Character, Long> characterLongMap;
        characterLongMap =  cardsAtHand.stream().collect(Collectors.groupingBy(PlayingCard::getSuit, Collectors.counting()));

        for (Map.Entry<Character, Long> pair : characterLongMap.entrySet()) {
            if (pair.getValue() >= (long) 5) {
                System.out.println(pair);
                c = pair.getKey();
                return true;
            }
        }
        return false;
    }

    /**
     * Gets a character from a variable
     *
     * @return a character
     */
    public Character getChar(){
        return c;
    }


    /**
     * Removes a Card from hand based on character
     *
     * @param ch to be removed
     */
    public void removeFromHand(Character ch){

        cardsAtHand.removeIf(p -> p.getSuit() == ch);
    }

    public boolean deckIsEmpty(){
       return this.deck.isDeckEmpty();

    }

}
