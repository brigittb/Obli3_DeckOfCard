
module Obli3_DeckOfCard_JavaFX_MAven {
    requires javafx.fxml;
    requires javafx.controls;
    requires javafx.graphics;
    opens edu.IDATT2001.brigittb.Obli3_DeckOfCard.GUI to javafx.fxml;
    exports edu.IDATT2001.brigittb.Obli3_DeckOfCard.GUI;
}
